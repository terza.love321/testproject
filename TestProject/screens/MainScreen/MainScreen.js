import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import FastImage from 'react-native-fast-image';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: [],
    };
  }

  componentDidMount() {
    this.fechProduct();
  }

  HeaderBar() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.onPressOption()}>
          <Image
            style={styles.iconOption}
            source={require('../../src/images/option.png')}
          />
        </TouchableOpacity>
        <Image
          style={styles.logoImg}
          source={require('../../src/images/logo-kinsaeb-3.png')}
        />
        <Image
          style={styles.iconOption}
          source={require('../../src/images/icon-shop2.png')}
        />
      </View>
    );
  }

  fechProduct() {
    fetch('https://kinzab.projectsoft.co.th/api/v1/product/')
      .then((response) => response.json())
      .then((json) => {
        // console.log(json.result);
        this.setState({
          product: json.result,
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  renderProductPresent() {
    // console.log(this.state.product)
    return this.state.product.map((data) => {
      if (data.product_new == 0) {
        return data.pic.map((img) => {
          return (
            <TouchableOpacity style={styles.productList} onPress={() => this.onPressProduct(data)}>
              <View>
                <FastImage
                  style={styles.imgProduct}
                  source={{
                    uri: `https://kinzab.projectsoft.co.th/api/v1/${img.pic_path}`,
                  }}
                />
              </View>
              <View>
                <Text style={styles.txtname}>{data.product_name}</Text>
                <Text style={styles.txtPrice}>₩ {data.product_price}</Text>
              </View>
            </TouchableOpacity>
          );
        });
      }
    });
  }

  renderProductNew() {
    return this.state.product.map((data) => {
      if (data.product_new == 1) {
        return data.pic.map((img) => {
          return (
            <TouchableOpacity
              style={styles.productList}
              onPress={() => this.onPressProduct(data)}>
              <View>
                <FastImage
                  style={styles.imgProduct}
                  source={{
                    uri: `https://kinzab.projectsoft.co.th/api/v1/${img.pic_path}`,
                  }}
                />
              </View>
              <View>
                <Text style={styles.txtname}>{data.product_name}</Text>
                <Text style={styles.txtPrice}>₩ {data.product_price}</Text>
              </View>
            </TouchableOpacity>
          );
        });
      }
    });
  }

  onPressProduct(data) {
    // console.log(data)
    this.props.navigation.navigate('ProducrScreen', {data});
  }
  onPressOption() {
    this.props.navigation.navigate('DrawerScreen');
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        {this.HeaderBar()}
        <ScrollView>
          <View style={styles.searchView}>
            <View style={styles.searchInput}>
              <Image
                style={{marginLeft: 10, width: 20, height: 20}}
                source={require('../../src/images/icon-search.png')}
              />
              <Text style={styles.textSearch}>ค้นหาสินค้า</Text>
            </View>
          </View>
          <View style={styles.content}>
            <Text style={styles.textHead}>สินค้าแนะนำ</Text>
            <ScrollView horizontal={true}>
              <View style={styles.productView}>
                {this.renderProductPresent()}
              </View>
            </ScrollView>
            <Text style={styles.textHead}>สินค้าใหม่</Text>
            <ScrollView horizontal={true}>
              <View style={styles.productView}>{this.renderProductNew()}</View>
            </ScrollView>
          </View>
          <View style={styles.footer}>
            <Image
              source={require('../../src/images/coverDrawer.png')}
              style={styles.imgFooter}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  searchView: {
    width: windowWidth,
    height: windowHeight / 14,
    backgroundColor: '#D20000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchInput: {
    width: '90%',
    backgroundColor: '#fff',
    borderRadius: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textSearch: {
    fontSize: 15,
    margin: 10,
  },

  content: {
    width: '100%',
    marginLeft: 10,
  },
  textHead: {
    fontSize: 20,
    marginVertical: 10,
  },
  productView: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  productList: {
    width: windowWidth / 2.2 - 20,
    backgroundColor: '#EDEDED',
    marginRight: 10,
  },
  imgProduct: {
    width: windowWidth / 2.2 - 20,
    height: windowWidth / 2.2 - 20,
  },
  txtname: {
    fontSize: 15,
    marginTop: 10,
    marginLeft: 5,
  },
  txtPrice: {
    fontSize: 18,
    color: '#D20000',
    marginLeft: 5,
    marginBottom: 5,
  },

  footer: {
    width: windowWidth,
    backgroundColor: '#D20000',
    alignItems: 'center',
  },
  imgFooter: {
    width: windowWidth / 4,
    height: windowWidth / 4,
  },

  container: {
    width: windowWidth,
    backgroundColor: '#D20000',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  iconOption: {
    width: windowWidth / 16,
    height: windowWidth / 16,
    marginHorizontal: 5,
  },
  logoImg: {
    width: windowWidth / 2,
    height: windowHeight / 15,
    marginVertical: 10,
  },
});
