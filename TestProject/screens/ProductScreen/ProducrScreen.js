import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

export default class ProducrScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: this.props.route.params.data,
      qty: 0,
    };
    // console.log('data', this.state.product);
    this.mapImage();
  }

  mapImage() {
    const {product} = this.state;
    return product.pic.map((img) => {
      //   console.log(img);
      return (
        <Image
          style={styles.imgProduct}
          source={{
            uri: `https://kinzab.projectsoft.co.th/api/v1/${img.pic_path}`,
          }}
        />
      );
    });
  }
  HeaderBar() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.onPressBack()}>
          <Image
            style={styles.iconOption}
            source={require('../../src/images/icon-arrow.png')}
          />
        </TouchableOpacity>
        <Image
          style={styles.logoImg}
          source={require('../../src/images/logo-kinsaeb-2.png')}
        />
        <Image
          style={styles.iconOption}
          source={require('../../src/images/icon-shop.png')}
        />
      </View>
    );
  }
  onPressBack() {
    this.props.navigation.navigate('MainScreen');
  }

  onPressPlus() {
    let counter;
    counter = this.state.qty + 1;
    this.setState({
      qty: counter,
    });
  }
  onPressDelete() {
    let counter;
    counter = this.state.qty - 1;
    this.setState({
      qty: counter,
    });
  }

  render() {
    const {product} = this.state;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        {this.HeaderBar()}
        <ScrollView>
          {this.mapImage()}
          <View style={styles.infoProductView}>
            <Text numberOfLines={2} style={styles.txtHead}>
              {product.product_name}
            </Text>
            <Text style={styles.txtTitle}>
              จำนวนสินค้า {product.product_quantity} ชิ้น
            </Text>
            <Text style={styles.txtPrice}>₩ {product.product_price}</Text>
            <Text style={styles.txtTitle} numberOfLines={3}>
              {product.product_detail}
            </Text>
          </View>
          <View style={styles.addNumberView}>
            <Text style={styles.txtHead}>จำนวน</Text>
            <View style={styles.processQtyView}>
              {this.state.qty == 0 ? (
                <View onPress={() => this.onPressDelete()}>
                  <Text style={{...styles.txtQtyButton, color: '#A9A9A9'}}>
                    -
                  </Text>
                </View>
              ) : (
                <TouchableOpacity onPress={() => this.onPressDelete()}>
                  <Text style={styles.txtQtyButton}>-</Text>
                </TouchableOpacity>
              )}

              <View style={styles.qtyView}>
                <Text style={styles.txtQty}>{this.state.qty}</Text>
              </View>
              <TouchableOpacity onPress={() => this.onPressPlus()}>
                <Text style={styles.txtQtyButton}>+</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        <TouchableOpacity style={styles.buttonAddToBasket}>
          <Text style={styles.txtButton}>เพิ่มลงตะกร้า</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  imgProduct: {
    width: windowWidth,
    height: windowWidth,
  },
  buttonAddToBasket: {
    width: windowWidth,
    height: windowHeight / 12,
    backgroundColor: '#D20000',
    alignItems: 'center',
    justifyContent: 'center',
  },
  txtButton: {
    fontSize: 18,
    color: '#fff',
  },
  infoProductView: {
    marginHorizontal: 25,
    marginVertical: 10,
  },
  txtHead: {
    fontSize: 20,
  },
  txtTitle: {
    fontSize: 14,
  },
  txtPrice: {
    fontSize: 25,
    color: '#D20000',
    marginVertical: 10,
  },
  addNumberView: {
    marginHorizontal: 25,
    marginVertical: 10,
  },
  processQtyView: {
    marginHorizontal: 10,
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txtQtyButton: {
    color: '#D20000',
    fontSize: 25,
  },
  qtyView: {
    width: windowWidth / 1.5,
    alignItems: 'center',
    // backgroundColor: 'red'
    borderBottomWidth: 0.5,
    justifyContent: 'center',
  },
  txtQty: {
    fontSize: 18,
  },

  container: {
    width: windowWidth,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  iconOption: {
    width: windowWidth / 16,
    height: windowWidth / 16,
    marginHorizontal: 5,
  },
  logoImg: {
    width: windowWidth / 2,
    height: windowHeight / 15,
    marginVertical: 10,
  },
});
