import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const dataButton = [
  {name: 'หน้าหลัก', id: 1},
  {name: 'รายการสั่งซื้อ', id: 2},
  {name: 'สินค้าแนะนำ', id: 3},
  {name: 'สินค้าใหม่', id: 4},
  {name: 'สินค้าทั้งหมด', id: 5},
  {name: 'วิธีชำระเงิน', id: 6},
];

export default class DrawerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onPressButton(data) {
    if (data.id == 1) {
      this.props.navigation.navigate('MainScreen');
    }
  }

  renderButtonDrawer(data) {
    console.log(data);
    return data.map((data) => {
      return (
        <TouchableOpacity
          style={styles.button}
          activeOpacity={0.5}
          onPress={() => this.onPressButton(data)}>
          <Text style={styles.txtButton}>{data.name}</Text>
        </TouchableOpacity>
      );
    });
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <Image
          style={styles.imgDrawer}
          source={require('../../src/images/coverDrawer.png')}
        />
        {this.renderButtonDrawer(dataButton)}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  imgDrawer: {
    width: windowWidth,
    height: '30%',
  },
  txtButton: {
    fontSize: 18,
    margin: 20,
  },
  button: {
    width: windowWidth,
    borderBottomWidth: 0.5,
  },
});
