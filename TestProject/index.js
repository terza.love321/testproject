/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import MainScreen from './screens/MainScreen/MainScreen'
import ProducrScreen from './screens/ProductScreen/ProducrScreen'
import DrawerScreen from './screens/DrawerScreen/DrawerScreen'
import {name as appName} from './app.json';


AppRegistry.registerComponent(appName, () => App);
