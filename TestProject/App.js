import React, {Component} from 'react';
import {View, Text} from 'react-native';

import MainScreen from './screens/MainScreen/MainScreen';
import ProducrScreen from './screens/ProductScreen/ProducrScreen';
import DrawerScreen from './screens/DrawerScreen/DrawerScreen';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default class App extends Component {
  CreateHomeStack = () => (
    <Stack.Navigator>
      <Stack.Screen name="MainScreen" component={MainScreen} />
      <Stack.Screen name="ProducrScreen" component={ProducrScreen} />
    </Stack.Navigator>
  );
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="MainScreen" component={MainScreen} />
          <Stack.Screen name="ProducrScreen" component={ProducrScreen} />
          <Stack.Screen name="DrawerScreen" component={DrawerScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
